import datetime

from django.http import JsonResponse
from django.utils.datetime_safe import datetime
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt

from team.models import Team
from team.utils import user_from_bearer
from .forms import DailyMoodForm
from .utils import get_team_mood_statistics


@csrf_exempt
def mood_api(request, team_id: int):
    """
    Mood API - handles both POST and GET, authenticated and unauthenticated.
    Always returns the statistics for the team.
    """

    try:
        team = Team.objects.get(id=team_id)
    except Team.DoesNotExist:
        return JsonResponse(data={"status": _("No team found.")}, status=404)

    date = datetime.today()

    status = 200
    body = {
        "team": team.as_dict(),
        "status": "No mood logged"
    }

    user = user_from_bearer(request.META.get('HTTP_AUTHORIZATION', None))

    if user and request.method == 'POST':
        if team in user.teams.all():
            if user.moods.filter(date=date).count() > 0:
                body["status"] = "Mood for today already exists."
            else:
                data = {
                    "happiness": request.POST.get("happiness"),
                    "user": user,
                    "team": team,
                    "date": date
                }
                form = DailyMoodForm(data)
                if form.is_valid():
                    form.save()
                    status = 201
                    body["status"] = _("Mood logged! Stay happy!")
                else:
                    body["status"] = _("Mood not logged - invalid input: %s" % form.errors.as_data())

    body["statistics"] = get_team_mood_statistics(team, date=date)
    return JsonResponse(data=body, status=status)
