from django.contrib import admin

from happy.models import Mood


class MoodAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'team', 'user', 'happiness', 'date']


admin.site.register(Mood, MoodAdmin)
