from django import forms

from happy.models import Mood


class DailyMoodForm(forms.ModelForm):

    class Meta:
        model = Mood
        fields = ['user', 'team', 'happiness', 'date']
