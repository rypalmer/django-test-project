import datetime
import json
from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from happy.models import Mood
from happy.utils import get_team_mood_statistics
from team.models import Team, TeamUser
from team.utils import get_jwt

user_model = get_user_model()


class BaseTestCase(TestCase):
    date: datetime.date
    user: TeamUser
    username = "test@example.com"
    password = "testpwdtestpwd"

    def setUp(self):
        self.date = datetime.date.today()
        self.user = user_model.objects.create_user(username=self.username, password=self.password)
        self.team = Team.objects.create(team_name='t')
        self.user.teams.add(self.team)


class MoodsTestCase(BaseTestCase):

    def test_get_team_mood_statistics_empty(self):
        date = datetime.date.today()
        stats = get_team_mood_statistics(self.team, date=date)
        self.assertEquals({str(i): 0 for i in range(1, 6)}, stats)

    def test_get_team_mood_statistics(self):
        yesterday = self.date - datetime.timedelta(days=1)
        self.user.moods.create(team=self.team, happiness=1, date=yesterday)
        self.user.moods.create(team=self.team, happiness=1, date=self.date)
        self.user.moods.create(team=self.team, happiness=2, date=self.date)
        self.user.moods.create(team=self.team, happiness=2, date=self.date)
        self.user.moods.create(team=self.team, happiness=4, date=self.date)
        stats = get_team_mood_statistics(self.team, date=self.date)
        self.assertEquals({'1': 1, '2': 2, '3': 0, '4': 1, '5': 0}, stats)


class MoodViewTestCase(BaseTestCase):
    test_stats = {'1': 1, '2': 4, '3': 5, '4': 2, '5': 8}

    def setUp(self):
        super().setUp()
        self.team2 = Team.objects.create(team_name='t2')
        self.jwt = get_jwt(self.user)

    def test_errors(self):
        # Bad team ID
        response = self.client.get(reverse("team_user_mood", args=(12345,)))
        self.assertEqual(response.status_code, 404)

    @mock.patch('happy.views.get_team_mood_statistics', mock.MagicMock(return_value=test_stats))
    def test_get_anon(self):
        response = self.client.get(reverse("team_user_mood", args=(self.team.id,)))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEquals(self.test_stats, data['statistics'])

    def test_submit_anon(self):
        data = {"happiness": 3}
        response = self.client.post(reverse("team_user_mood", args=(self.team.id,)), data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, Mood.objects.count())

    def test_submit_authenticated_wrong_team(self):
        response = self.client.post(
            reverse("team_user_mood", args=(self.team2.id,)),
            {},
            HTTP_AUTHORIZATION='Bearer %s' % self.jwt
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, Mood.objects.count())

    def test_submit_invalid_data(self):
        data = {"happiness": 6}
        response = self.client.post(
            reverse("team_user_mood", args=(self.team.id,)),
            data,
            HTTP_AUTHORIZATION='Bearer %s' % self.jwt
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Mood not logged - invalid input")

    def test_submit_authenticated(self):
        data = {"happiness": 3}
        response = self.client.post(
            reverse("team_user_mood", args=(self.team.id,)),
            data,
            HTTP_AUTHORIZATION='Bearer %s' % self.jwt
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(1, Mood.objects.count())
        mood = Mood.objects.latest()
        self.assertEquals(3, mood.happiness)
        self.assertEquals(self.user, mood.user)
        self.assertEquals(self.team, mood.team)
        self.assertEquals(self.date, mood.date)
        data = json.loads(response.content)
        self.assertEqual(data['statistics']['1'], 0)
        self.assertEqual(data['statistics']['3'], 1)
