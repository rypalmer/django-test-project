import datetime

from django.db.models import Count

from team.models import Team


def get_team_mood_statistics(team: Team, date: datetime.date) -> dict:
    """
    Return a dict of Happiness counts for Mood data for a Team. Happiness ranges from 1-5.
    This would be a method on Team model, but its preferable to not introduce dependencies between apps
    """
    results = {str(i): 0 for i in range(1, 6)}
    data = team.moods.filter(date=date).values_list("happiness").annotate(num_users=Count("user"))
    for d in data:
        results[str(d[0])] = d[1]
    return results
