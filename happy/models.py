from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from team.models import Team

user_model = get_user_model()


class Mood(models.Model):
    user = models.ForeignKey(user_model, on_delete=models.CASCADE, related_name="moods")
    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="moods")
    # happiness level from 1 (Unhappy) to 3 (Neutral) to 5 (Very Happy)
    happiness = models.IntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)],
        choices=[(i, i) for i in range(1, 6)]
    )
    date = models.DateField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        get_latest_by = 'id'

    def __str__(self):
        return "%s's happiness is a %d on %s" % (self.user, self.happiness, self.date)
