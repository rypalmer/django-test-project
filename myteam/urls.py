"""myteam URL Configuration
"""
from django.contrib import admin
from django.urls import path

from happy import views as happy_views
from team import views as team_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login', team_views.login, name='login'),
    path('team/<int:team_id>/mood', happy_views.mood_api, name="team_user_mood")
]
