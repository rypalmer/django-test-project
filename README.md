# How is My Team?
A simple Django application to allow daily checkins and monitoring of your teams happiness.

## Requirements

Create a simple Django **RESTful** application that:

1. Provides an API that allows users make authenticated requests.
2. Once per day, users can submit their happiness level from 1 (Unhappy) to 3 (Neutral) to 5 (Very Happy).
3. Upon receiving the request, return the statistics information: number of people at each level and the average happiness of the team.
4. If an unauthenticated request is made to the same endpoint, return the stastistics.
5. New users can be added via the Django admin.
6. Bonus: SaaS! Users can belong to teams and only their teams stats are returned.

Note: If something is not clear in the above requirements, just leave an explanation in the code (where you made a decision), or make it clear from the code what you mean to do.

## Guidelines

* You MUST include installation instructions so that it can be run locally be other developers.
* You MUST document any applications or external packages you use.
* You SHOULD be following Django best practices.
* You SHOULD take as little or as long as you need (but don't overdo it). You will not be evaluated on time to complete.
* You SHOULD ask questions if anything specified here is not clear in any way.
* You SHOULD incrementally commit to this repository along the way.

## Instructions

1. Fork this repository using your personal account.
2. Create your solution. Test it. Test it again to be sure. Commit it and push to your personal repo.
3. Email us the link to your fork.

## Evaluation Criteria

You will be evaluated with the following in mind:

* Does the solution satisfy the five (or 6) requirements?
* Does the solution run locally based on the provided instructions?
* Does the solution make good use of tools/frameworks/libraries/APIs?
* Does the implementation follow established best practices (design patterns, language usage, code formatting, etc..)?


## Dependencies

### Environment
* Python 3.7, with virtualenv support
* SQLite

## Installation

Using a Virtual Environment is recommended. Modify if virtualenv or python3 are not in your $PATH.

    virtualenv -p python3 venv
    source venv/bin/activate
    (venv) pip install -r requirements.txt

#### Packages installed

* Django 3.0.x
* PyJWT 1.7.x

### Generate a SECRET_KEY
Generate a new secret key using this command:

    python -c 'import random; result = "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)") for i in range(50)]); print(result)'

Modify myteam/settings.py to set the output as your personal SECRET_KEY. This value is also used as the JSON Web Token encoding/decoding secret key.

### Database (SQLite) initialization

    (venv) ./manage.py migrate
    (venv) ./manage.py createsuperuser

Follow prompts to create a new Superuser. Also create a Team, a User, and associate the User with the Team on the User Admin form.

## Running tests

    (venv) ./manage.py test

## Running a test server locally

    (venv) ./manage.py runserver

Browse to http://127.0.0.1:8000/admin/ and log in as a superuser.

## Creating users & authenticating them

Using the Django admin for Team Users, create a user: http://127.0.0.1:8000/admin/team/teamuser/add/

To generate a reusable token for further API authentication, POST a user's username & password to /login: 

    curl --data "username=test&password=test1" http://127.0.0.1:8000/login

Example response:

    {"status": "You are now logged in.", "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0ZWFtX3VzZXJfaWQiOjIsIm9yaWdfaWF0IjoxNTgzNzMzODk5fQ.i22IpqmQAiCnSemssO3JIYdlwcaIkBLkxhwWTfy4UD0", "teams": [{"id": 1, "team_name": "Test Team 1"}]}

The response contains the list of teams the user belongs to.

You can decode the JSON Web Token (JWT) here: https://jwt.io/

The token does not expire. To authenticate using the token, pass it as a Bearer token Authorization:

    curl -H "Authorization: Bearer <token>"

## Logging moods

Moods are snapshots of team members' happiness, with a happiness value of 1 to 5. Users may only log one value per day.  

### Anonymous user

Attempting to log a Mood as an anonymous user returns that team's stats and does not save the Mood:

    curl --data "happiness=2" http://127.0.0.1:8000/team/1/mood

Response:

    {"team": {"id": 1, "team_name": "Test Team 1"}, "status": "No mood logged", "statistics": {"1": 0, "2": 0, "3": 0, "4": 0, "5": 0}}
    
### Reusing the bearer token from the login above to authenticate and log a Mood with a Happiness of "2"

    curl -H 'Accept: application/json' -H "Authorization: Bearer <token>" --data "happiness=2" http://127.0.0.1:8000/team/1/mood
    
Response:

    {"team": {"id": 1, "team_name": "Test Team 1"}, "status": "Mood logged! Stay happy!", "statistics": {"1": 0, "2": 1, "3": 0, "4": 0, "5": 0}}

### Attempting to log a Mood more than once per day will not log subsequent Moods.

    {"team": {"id": 1, "team_name": "Test Team 1"}, "status": "Mood for today already exists.", "statistics": {"1": 0, "2": 1, "3": 0, "4": 0, "5": 0}}