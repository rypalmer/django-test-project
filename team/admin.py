from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import TeamUser, Team


class TeamsInline(admin.TabularInline):
    model = Team.members.through


class TeamUserAdmin(UserAdmin):
    inlines = [
        TeamsInline
    ]


admin.site.register(TeamUser, TeamUserAdmin)


class TeamAdmin(admin.ModelAdmin):
    pass


admin.site.register(Team, TeamAdmin)
