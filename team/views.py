from django.contrib.auth import authenticate
from django.http import JsonResponse
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from team.utils import get_jwt


@require_POST
@csrf_exempt
def login(request):
    """
    Authentication API
    """

    if not request.POST.get('username') or not request.POST.get('password'):
        return JsonResponse(status=400, data={"status": _("Bad request: missing username or password.")})

    user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
    if user is not None:
        return JsonResponse(
            {
                "status": _("You are now logged in."),
                "token": get_jwt(user),
                "teams": [
                    t.as_dict() for t in user.teams.all()
                ]
            })

    return JsonResponse(status=401, data={"status": _("Incorrect username/password combination.")})
