import datetime
from calendar import timegm
from typing import Optional

import jwt
from django.conf import settings
from jwt import DecodeError

from team.models import TeamUser


def get_jwt(user: TeamUser) -> str:
    payload = dict(
        team_user_id=user.id,
        orig_iat=timegm(datetime.datetime.utcnow().utctimetuple())
    )
    return jwt.encode(
        payload,
        settings.JWT_PRIVATE_KEY,
        settings.JWT_ALGORITHM
    ).decode('utf-8')


def jwt_decode(token: str) -> dict:
    """
    Will raise an Expiration exception if appropriate.
    """
    decoded_jwt = jwt.decode(
        token,
        settings.JWT_PRIVATE_KEY,
        options={'verify_exp': False},
        algorithms=[settings.JWT_ALGORITHM],
    )
    return decoded_jwt


def user_from_bearer(token: Optional[str]) -> Optional[TeamUser]:
    """
    Given a bearer token string, return a TeamUser object if it is valid.
    Return None otherwise.
    """
    if token is None:
        return None

    jwt_token = token.strip().replace('Bearer ', '')
    try:
        decoded_jwt = jwt_decode(jwt_token)
    except DecodeError:
        return None

    try:
        user = TeamUser.objects.get(id=decoded_jwt['team_user_id'])
        return user
    except TeamUser.DoesNotExist:
        return None
