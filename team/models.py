from django.contrib.auth.models import AbstractUser
from django.db import models


class Team(models.Model):
    """
    Team model for associating groups of users
    """
    team_name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Team #%d: %s' % (self.id, self.team_name)

    def as_dict(self):
        return {
            "id": self.id,
            "team_name": self.team_name
        }


class TeamUser(AbstractUser):
    """
    Override the basic auth user model so we can add a FK reference to their Team
    """
    teams = models.ManyToManyField(Team, related_name='members')
