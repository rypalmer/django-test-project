import json

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from team.models import TeamUser, Team
from team.utils import get_jwt, jwt_decode, user_from_bearer

user_model = get_user_model()


class ModelsTestCase(TestCase):

    def test_as_dict(self):
        team = Team.objects.create(team_name='t')
        self.assertEquals(
            {"id": team.id, "team_name": team.team_name},
            team.as_dict()
        )


class LoginApiTestCase(TestCase):
    username = "test@example.com"
    password = "testpwdtestpwd"

    def setUp(self):
        self.user = user_model.objects.create_user(username=self.username, password=self.password)

    def test_errors(self):
        response = self.client.get(reverse("login"), None)
        self.assertEquals(response.status_code, 405)

        response = self.client.post(reverse("login"), None)
        self.assertEqual(response.status_code, 400)

        response = self.client.post(reverse("login"), {'username': self.username})
        self.assertEqual(response.status_code, 400)

        response = self.client.post(reverse("login"), {'password': self.username})
        self.assertEqual(response.status_code, 400)

        response = self.client.post(reverse("login"), {'username': self.username, 'password': 'badpwd'})
        self.assertEqual(response.status_code, 401)

    def test_success(self):
        response = self.client.post(reverse("login"), {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertTrue('token' in response_data)
        jwt_decoded = jwt_decode(response_data['token'])
        self.assertEquals(self.user.id, jwt_decoded['team_user_id'])

    def test_teams_for_user(self):
        response = self.client.post(reverse("login"), {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.content)
        self.assertTrue('teams' in response_data)
        self.assertEquals(response_data['teams'], [])
        team = Team.objects.create(team_name='t')
        self.user.teams.add(team)
        response = self.client.post(reverse("login"), {'username': self.username, 'password': self.password})
        response_data = json.loads(response.content)
        self.assertEquals(1, len(response_data['teams']))
        self.assertEquals(team.id, response_data['teams'][0]['id'])
        self.assertEquals(team.team_name, response_data['teams'][0]['team_name'])


class UtilsTestCase(TestCase):
    user: TeamUser

    def setUp(self):
        self.user = TeamUser.objects.create(id=123)

    def test_get_jwt_decode_jwt(self):

        jwt = get_jwt(self.user)

        jwt_decoded = jwt_decode(jwt)
        self.assertEquals(self.user.id, jwt_decoded['team_user_id'])

    def test_user_from_bearer(self):
        self.assertIsNone(user_from_bearer(None))
        self.assertIsNone(user_from_bearer(''))
        self.assertIsNone(user_from_bearer('Bogus'))

    def test_user_from_bearer_user(self):
        token = get_jwt(self.user)
        self.assertEqual(self.user, user_from_bearer("Bearer %s" % token))
